package main

import (
	"fmt"
	"math"
	"math/rand"

	//"sync"
	"time"
)

func estimatePi(numDartsGiven int64) float64 {
	var numThrown, numBullseyes int64
	var insidePercentage float64
	seed := rand.NewSource(time.Now().UnixNano())
	random := rand.New(seed)
	for numDartsGiven >= numThrown { //while the number of darts given is greater than the number thrown, continue.
		x := random.Float64()
		y := random.Float64()
		if (x*x)+(y*y) < 1.0 {
			numBullseyes += 1 //if >1 It the dart is inside the circle
		}
		numThrown += 1 //add one to number of darts thrown
	}
	insidePercentage = (float64(numBullseyes) / float64(numDartsGiven)) * 4.0 //cast to a float
	return insidePercentage
}

//*****************ITERATION TWO*************************************************
//takes in the Number of darts and starts a timer to calculate how long it will take
//calls estimate Pi on the number of darts stored in the piEstimate variable
//prints out the disparity between the result and the real value of Pi
func iterationTwo(numDarts int64) int {
	var piEstimate float64 = estimatePi(numDarts) //calling estimate Pi
	var disparity = math.Abs(math.Pi - piEstimate)
	fmt.Println("Throwing ", numDarts, " this run, got within ", disparity, " of PI") //printing disparity
	return 0
}
func printElapsedTime(start time.Time) {
	var elapsed = time.Since(start)
	fmt.Println("took", elapsed)
}

func main() {
	//****START MULTI THREAD VERSION
	/*
			//helper function to set up concurrency
		   	var wg sync.WaitGroup
		   	printPi := func(which int64) {
		   		defer wg.Done()
		   		fmt.Println(iterationTwo(which))
		   	}

		   	defer printElapsedTime(time.Now())
		   	wg.Add(5) //adding the threads
		   	go printPi(100000)
		   	go printPi(1000000)
		   	go printPi(10000000)
		   	go printPi(10000000)
		   	go printPi(100000000)
		   	wg.Wait()
		   	fmt.Println("done with multithreading")
		   }
	*/ //END MULTI THREADED VERSION

	//*************SINGLE THREAD VERSION
	defer printElapsedTime(time.Now()) //The Timer
	iterationTwo(100000)
	iterationTwo(1000000)
	iterationTwo(10000000)
	iterationTwo(100000000)

}
